﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IznajmiAPI.DtoMappers
{
    public static class SobaDto
    {
        public static Models.Soba FromJson(JObject json){
            var brojSobe = json["broj_sobe"].ToObject<int>();
            var objekt = json["objekt_id"].ToObject<int>();


            var idSobe = json["soba_id"].ToObject<int?>();
            int id;
            if (idSobe == null )
            {
                id = 0;
            }
            else { id = (int)idSobe; }

            return new Models.Soba(id, brojSobe, objekt, new Models.Objekt(0, ""));
        }

        public static Models.Soba PutMethodFromJson(JObject json)
        {
            var brojSobe = json["broj_sobe"].ToObject<int>();
            var objekt = json["objekt_id"].ToObject<int?>();
            var idSobe = json["soba_id"].ToObject<int?>();

            return new Models.Soba(idSobe, brojSobe, objekt, new Models.Objekt(0, ""));
        }
    }
}