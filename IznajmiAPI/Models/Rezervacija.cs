﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IznajmiAPI.Models
{
    public class Rezervacija
    {
        public int? Id_rezervacija { get; set; }
        public int Id_gost_foreignKey { get; set; }
        public int Id_soba_foreignKey { get; set; }
        public DateTime DatumOd { get; set; }
        public DateTime DatumDo { get; set; }

        public Models.Gost Gost { get; set; }

        public Models.Soba Soba { get; set; }
        


        public Rezervacija(int? id_rezervacija, int id_gost_fk, int id_soba_fk, DateTime datumod, DateTime datumdo, Models.Gost g, Models.Soba s)
        {
            this.Id_rezervacija = id_rezervacija;
            this.Id_gost_foreignKey = id_gost_fk;
            this.Id_soba_foreignKey = id_soba_fk;
            this.DatumOd = datumod;
            this.DatumDo = datumdo;
            this.Gost = g;
            this.Soba = s;
            
        }
    }
}
