﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Net.Http;
using System.Text;
using System.Windows.Forms;
using DesktopApp.Models;
using Newtonsoft.Json;

namespace DesktopApp
{
    public partial class FormSobe : Form
    {
        public FormSobe()
        {
            InitializeComponent();
        }

        // Create HttpClient
        public HttpClient client = new HttpClient { BaseAddress = new Uri("https://localhost:44315/") };

        public Soba[] sobe;
        public Objekt[] objekti;

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private async void button3_Click(object sender, EventArgs e)
        {
            //button Dodaj Sobu
            if(listBox2.SelectedItem == null)
            {
                MessageBox.Show("Potrebno je odabrati objekt u kojem je nova soba.");
                return;
            }
            int unos;
            try
            {
                unos = int.Parse(textBox1.Text);
            }
            catch
            {
                MessageBox.Show("Potrebno je unijeti broj sobe!");
                return;
            }
            var idObjekta =int.Parse(listBox2.SelectedItem.ToString().Split(' ')[0]);

            var o = new { broj_sobe = unos, objekt_id = idObjekta };

            var json = JsonConvert.SerializeObject(o);
            var a = new StringContent(json, Encoding.UTF8, "application/json");
            var result = await client.PostAsync("api/soba", a);
            if (result.StatusCode.ToString() == "OK")
            {
                MessageBox.Show("Success");
            }
            else
            {
                MessageBox.Show("Unsuccessful");
                return;
            }

            FormSobe fr = new FormSobe();
            this.Close();
            fr.Show();
            fr.TopMost = true;

        }

        private async void FormSobe_Load(object sender, EventArgs e)
        {
            listBox1.Items.Add("please wait...");
            listBox2.Items.Add("please wait...");

            var responseMessage = await client.GetAsync("api/soba");

            var resultArray = await responseMessage.Content.ReadAsStringAsync();

            sobe = JsonConvert.DeserializeObject<Soba[]>(resultArray);
            listBox1.Items.Clear();
            foreach (var item in sobe)
            {
                listBox1.Items.Add(item.Id_soba + " \tBROJ SOBE: " + item.BrojSobe + " \tOBJEKT: " + item.Objekt.Naziv);
            }

            var rm = await client.GetAsync("api/objekt");
            var res = await rm.Content.ReadAsStringAsync();
            objekti = JsonConvert.DeserializeObject<Objekt[]>(res);
            listBox2.Items.Clear();

            foreach (var item in objekti)
            {
                listBox2.Items.Add(item.Id_objekt + " " + item.Naziv);
            }
        }

        private async void button4_Click(object sender, EventArgs e)
        {
            //button Izbrisi sobu
            if(listBox1.SelectedItem == null)
            {
                MessageBox.Show("Nije odabrana soba za brisanje.");
                return;
            }
            var odabrana = listBox1.SelectedItem.ToString().Split(' ');
            int id = int.Parse(odabrana[0]);

            var result = await client.DeleteAsync("api/soba/" + id);
            if (result.StatusCode.ToString() == "OK")
            {
                MessageBox.Show("Success");
            }
            else
            {
                MessageBox.Show("Unsuccessful");
            }
            FormSobe fr = new FormSobe();
            this.Close();
            fr.Show();
            fr.TopMost = true;
        }

        private async void button2_Click(object sender, EventArgs e)
        {
            //button azuriraj
            if(listBox1.SelectedItem == null)
            {
                MessageBox.Show("Nije odabrana soba.");
                return;
            }
            int sobaId =int.Parse( listBox1.SelectedItem.ToString().Split(' ')[0]);
            int? noviObjekt = null;
            if (listBox2.SelectedItem != null)
            {
                noviObjekt = int.Parse(listBox2.SelectedItem.ToString().Split(' ')[0]);
            }
            
            int noviBrojSobe =int.Parse( textBox2.Text);

            var obj = new { soba_id = sobaId, broj_sobe = noviBrojSobe, objekt_id = noviObjekt };

            var json = JsonConvert.SerializeObject(obj);
            var a = new StringContent(json, Encoding.UTF8, "application/json");
            var result = await client.PutAsync("api/soba", a);
            if (result.StatusCode.ToString() == "OK")
            {
                MessageBox.Show("Success");
            }
            else
            {
                MessageBox.Show("Unsuccessful");
                return;
            }

            FormSobe fr = new FormSobe();
            this.Close();
            fr.Show();
            fr.TopMost = true;
        }
    }
}