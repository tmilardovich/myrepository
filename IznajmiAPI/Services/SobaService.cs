﻿using IznajmiAPI.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IznajmiAPI.Services
{
    public class SobaService
    {
        private SobaRepository _sobaRepo;

        public SobaService(SobaRepository s)
        {
            _sobaRepo = s;
        }

        public List<Models.Soba> DohvatiSobe()
        {
            return _sobaRepo.DohvatiSveSobeIObjekte();
        }

        public void BrisanjeSobe(int id)
        {
            _sobaRepo.BrisanjeSobe(id);
        }

        public void DodajSobu(DbModels.Soba s)
        {
            _sobaRepo.DodavanjeSobe(s);
        }

        public void AzurirajPodatkeSobe(Models.Soba s)
        {
            _sobaRepo.AzuriranjePodatakaSobe(s);
        }
    }
}