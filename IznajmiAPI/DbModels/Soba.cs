﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IznajmiAPI.DbModels
{
    public partial class Soba
    {
        public Soba()
        {
            Rezervacijas = new HashSet<Rezervacija>();
        }

        public int IdSoba { get; set; }
        public int Broj { get; set; }
        public int IdObjekt { get; set; }

        public virtual Objekt IdObjektNavigation { get; set; }
        public virtual ICollection<Rezervacija> Rezervacijas { get; set; }
    }
}
