﻿using IznajmiAPI.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using IznajmiAPI.Models;
namespace IznajmiAPI.Services
{
    public class RezervacijeService
    {
        private RezervacijeRepository _rezervacijeRepo;

        public RezervacijeService(RezervacijeRepository r)
        {
            _rezervacijeRepo = r;
        }

        public List<Models.Rezervacija> GetAll()
        {
            return _rezervacijeRepo.GetAll();
        }

        public void SpremanjeRezervacije(DbModels.Rezervacija r)
        {
            _rezervacijeRepo.DodavanjeRezervacije(r);
        }

        public void Brisanje(int id)
        {
            _rezervacijeRepo.BrisanjeRezervacije(id);
        }

        public void Azuriranje(DbModels.Rezervacija r)
        {
            _rezervacijeRepo.AzuriranjeRezervacije(r);
        }
    }
}
