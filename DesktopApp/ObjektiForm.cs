﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Net.Http;
using System.Text;
using System.Windows.Forms;
using DesktopApp.Models;
using System.Net.Http.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace DesktopApp
{
    public partial class ObjektiForm : Form
    {
        public ObjektiForm()
        {
            InitializeComponent();
        }

        public Objekt[] objekti;

        public HttpClient client = new HttpClient { BaseAddress = new Uri("https://localhost:44315/") };

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private async void ObjektiForm_Load(object sender, EventArgs e)
        {
            listBox1.Items.Add("please wait");

            var responseMessage = await client.GetAsync("api/objekt");
            var resultArray = await responseMessage.Content.ReadAsStringAsync();

            objekti = JsonConvert.DeserializeObject<Objekt[]>(resultArray);
            listBox1.Items.Clear();
            foreach (var item in objekti)
            {
                listBox1.Items.Add(item.Id_objekt + " " + item.Naziv + "\n");
            }
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblSelected.Text = listBox1.SelectedItem.ToString();
        }

        private async void btnDodaj_Click(object sender, EventArgs e)
        {
            var o = new { naziv = textBox1.Text, id_objekt = 0 };

            var json = JsonConvert.SerializeObject(o);
            var a = new StringContent(json, Encoding.UTF8, "application/json");
            var result = await client.PostAsync("api/objekt", a);
            if (result.StatusCode.ToString() == "OK")
            {
                MessageBox.Show("Success");
            }
            else
            {
                MessageBox.Show("Unsuccessful");
                return;
            }

            ObjektiForm fr = new ObjektiForm();
            this.Close();
            fr.Show();
            fr.TopMost = true;
        }

        private async void btnBrisi_Click(object sender, EventArgs e)
        {
            var odabran = listBox1.SelectedItem;
            if (odabran == null)
            {
                MessageBox.Show("Nije odabran objekt");
                return;
            }
            var niz = listBox1.SelectedItem.ToString().Split(' ');
            string id = niz[0];

            var result = await client.DeleteAsync("api/objekt/" + id);
            if (result.StatusCode.ToString() == "OK")
            {
                MessageBox.Show("Success");
            }
            else
            {
                MessageBox.Show("Unsuccessful");

            }
            ObjektiForm fr = new ObjektiForm();
            this.Close();
            fr.Show();
            fr.TopMost = true;
        }

        private async void btnAzuriraj_Click(object sender, EventArgs e)
        {
            var odabran = listBox1.SelectedItem;
            if(odabran == null)
            {
                MessageBox.Show("Nije odabran objekt");
                return;
            }
            var odabrani = listBox1.SelectedItem.ToString().Split(' ');
            
            var id = odabrani[0];

            var obj = new { naziv = textBox1.Text, id_objekt = id };

            var json = JsonConvert.SerializeObject(obj);
            var a = new StringContent(json, Encoding.UTF8, "application/json");
            var result = await client.PutAsync("api/objekt",a);
            if (result.StatusCode.ToString() == "OK")
            {
                MessageBox.Show("Success");
            }
            else
            {
                MessageBox.Show("Unsuccessful");
                return;
            }

            ObjektiForm fr = new ObjektiForm();
            this.Close();
            fr.Show();
            fr.TopMost = true;

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }
    }
}
