﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Net.Http;
using System.Text;
using System.Windows.Forms;
using DesktopApp.Models;
using Newtonsoft.Json;


namespace DesktopApp
{
    public partial class FormNovaRezervacija : Form
    {

        public HttpClient client = new HttpClient { BaseAddress = new Uri("https://localhost:44315/") };

        public Rezervacija[] rezervacije;
        public Objekt[] objekti;
        public Soba[] sobe;
        public Gost[] gosti;
        public FormNovaRezervacija()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private async void FormNovaRezervacija_Load(object sender, EventArgs e)
        {
            var responseMsgGosti = await client.GetAsync("api/gost");

            var resultArrayGosti = await responseMsgGosti.Content.ReadAsStringAsync();

            gosti = JsonConvert.DeserializeObject<Gost[]>(resultArrayGosti);
            foreach (var item in gosti)
            {
                comboGost.Items.Add(item.Id_gost + " " + item.Ime + " " + item.Prezime);
            }

            var responseMsgObjekti = await client.GetAsync("api/objekt");

            var resultArrayObjekti = await responseMsgObjekti.Content.ReadAsStringAsync();

            objekti = JsonConvert.DeserializeObject<Objekt[]>(resultArrayObjekti);
            foreach (var item in objekti)
            {
                comboObjekti.Items.Add(item.Id_objekt + " " + item.Naziv);
            }

            var responseMsgSobe = await client.GetAsync("api/soba");
            var resultArraySobe = await responseMsgSobe.Content.ReadAsStringAsync();
            sobe = JsonConvert.DeserializeObject<Soba[]>(resultArraySobe);

            listBox1.Items.Add("please wait...");
            var responseMessage = await client.GetAsync("api/rezervacije");

            var resultArray = await responseMessage.Content.ReadAsStringAsync();

            rezervacije = JsonConvert.DeserializeObject<Rezervacija[]>(resultArray);
            listBox1.Items.Clear();
            foreach (var item in rezervacije)
            {
                listBox1.Items.Add(item.Id_rezervacija + " \t" + 
                    item.Soba.BrojSobe + "("+item.Soba.Objekt.Naziv+")"+ " \t" + 
                    item.DatumOd.ToString().Split(' ')[0] + "-" + item.DatumDo.ToString().Split(' ')[0] + " " + 
                    item.Gost.Ime + " " + item.Gost.Prezime);
            }
           }
        
        private void comboObjekti_SelectedIndexChanged(object sender, EventArgs e)
        {
            comboSobe.Items.Clear();
            foreach (var soba in sobe)
            {
                if(soba.Id_objekt_foreignKey == int.Parse(comboObjekti.SelectedItem.ToString().Split(' ')[0]))
                {
                    comboSobe.Items.Add(soba.Id_soba + " \tBroj sobe: " + soba.BrojSobe);
                }
            }
        }

        private async void txtIzbrisi_Click(object sender, EventArgs e)
        {
            //button brisi
            if (listBox1.SelectedItem == null)
            {
                MessageBox.Show("Niste odabrali rezervaciju za brisanje.");
                return;
            }
            var id = int.Parse(listBox1.SelectedItem.ToString().Split(' ')[0]);

            var result = await client.DeleteAsync("api/rezervacije/" + id);
            if (result.StatusCode.ToString() == "OK")
            {
                MessageBox.Show("Success");
            }
            else
            {
                MessageBox.Show("Unsuccessful");
            }
            FormNovaRezervacija fr = new FormNovaRezervacija();
            this.Close();
            fr.Show();
            fr.TopMost = true;
        }

        private async void btnDodaj_Click(object sender, EventArgs e)
        {
            if(comboGost.Text == "")
            {
                MessageBox.Show("Nije odabran gost.");
                return;
            }
            if (comboObjekti.Text == "")
            {
                MessageBox.Show("Nije odabran objekt.");
                return;
            }
            if (comboSobe.Text == "")
            {
                MessageBox.Show("Nije odabrana soba.");
                return;
            }

            var obj = new { datum_od = dateTimePicker1.Value.ToString("yyyy-MM-dd"),
                datum_do = dateTimePicker2.Value.ToString("yyyy-MM-dd"),
                user_id = int.Parse(comboGost.Text.ToString().Split(' ')[0]),
                soba_id = int.Parse(comboSobe.Text.ToString().Split(' ')[0]),
                rezervacija_id = 0
            };

            var json = JsonConvert.SerializeObject(obj);
            var a = new StringContent(json, Encoding.UTF8, "application/json");
            var result = await client.PostAsync("api/rezervacije", a);
            if (result.StatusCode.ToString() == "OK")
            {
                MessageBox.Show("Success");
            }
            else
            {
                MessageBox.Show("Unsuccessful");
                return;
            }

            FormNovaRezervacija fr = new FormNovaRezervacija();
            this.Close();
            fr.Show();
            fr.TopMost = true;
        }

        private async void btnAzuriraj_Click(object sender, EventArgs e)
        {
            //azuriranje
            if(listBox1.SelectedItem == null)
            {
                MessageBox.Show("Nije odabrana rezervacija za azuriranje.");
                return;
            }
            if (comboGost.Text == "")
            {
                MessageBox.Show("Nije odabran gost.");
                return;
            }
            if (comboObjekti.Text == "")
            {
                MessageBox.Show("Nije odabran objekt.");
                return;
            }
            if (comboSobe.Text == "")
            {
                MessageBox.Show("Nije odabrana soba.");
                return;
            }

            var obj = new
            {
                datum_od = dateTimePicker1.Value.ToString("yyyy-MM-dd"),
                datum_do = dateTimePicker2.Value.ToString("yyyy-MM-dd"),
                user_id = int.Parse(comboGost.Text.ToString().Split(' ')[0]),
                soba_id = int.Parse(comboSobe.Text.ToString().Split(' ')[0]),
                rezervacija_id = int.Parse(listBox1.SelectedItem.ToString().Split(' ')[0])
            };

            var json = JsonConvert.SerializeObject(obj);
            var a = new StringContent(json, Encoding.UTF8, "application/json");
            var result = await client.PutAsync("api/rezervacije", a);
            if (result.StatusCode.ToString() == "OK")
            {
                MessageBox.Show("Success");
            }
            else
            {
                MessageBox.Show("Unsuccessful");
                return;
            }

            FormNovaRezervacija fr = new FormNovaRezervacija();
            this.Close();
            fr.Show();
            fr.TopMost = true;

        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            var podaci = listBox1.SelectedItem.ToString().Split(' ');
            var idRezervacije = int.Parse(podaci[0]);
            Rezervacija rez = rezervacije[0];
            foreach (var item in rezervacije)
            {
                if(item.Id_rezervacija == idRezervacije)
                {
                    rez = item;
                }
            }
            comboGost.SelectedItem = rez.Gost.Id_gost + " " +rez.Gost.Ime + " " + rez.Gost.Prezime;
            comboObjekti.SelectedItem = rez.Soba.Objekt.Id_objekt + " " + rez.Soba.Objekt.Naziv;
            comboSobe.SelectedItem = rez.Soba.Id_soba + " \tBroj sobe: " + rez.Soba.BrojSobe;
            dateTimePicker1.Value = rez.DatumOd;
            dateTimePicker2.Value = rez.DatumDo;
        }
    }
}