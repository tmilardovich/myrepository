﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesktopApp.Models
{
    public class Objekt
    {
        public int? Id_objekt { get; set; }
        public string Naziv { get; set; }

        public Objekt(int? id_objekt, string naziv)
        {
            this.Id_objekt = id_objekt;
            this.Naziv = naziv;

        }
    }
}
