﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Net.Http;
using System.Text;
using System.Windows.Forms;
using DesktopApp.Models;

namespace DesktopApp
{
    public partial class GostiForm : Form
    {
        public GostiForm()
        {
            InitializeComponent();
        }
        // Create HttpClient
        public HttpClient client = new HttpClient { BaseAddress = new Uri("https://localhost:44315/") };

        public Gost[] gosti;

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private async void GostiForm_Load(object sender, EventArgs e)
        {
            listBox1.Items.Add("please wait");
            var responseMessage = await client.GetAsync("api/gost");

            var resultArray = await responseMessage.Content.ReadAsStringAsync();

            // Deserialize the Json string into type using JsonConvert
            gosti = JsonConvert.DeserializeObject<Gost[]>(resultArray);
            listBox1.Items.Clear();
            foreach (var item in gosti)
            {
                listBox1.Items.Add(item.Id_gost + " " + item.Ime + " "+ item.Prezime + "\n");
            }
        }

        private async void button3_Click(object sender, EventArgs e)
        {
            //btn dodaj gosta
            var ime = textBox1.Text;
            var prezime = textBox2.Text;

            if(ime == "" || prezime == "")
            {
                MessageBox.Show("Niste unijeli ime i/ili prezime gosta.");
                return;
            }
            var obj = new { ime = ime, prezime = prezime, id = 0 };

            var json = JsonConvert.SerializeObject(obj);
            var a = new StringContent(json, Encoding.UTF8, "application/json");
            var result = await client.PostAsync("api/gost", a);
            if (result.StatusCode.ToString() == "OK")
            {
                MessageBox.Show("Success");
            }
            else
            {
                MessageBox.Show("Unsuccessful");
                return;
            }

            GostiForm fr = new GostiForm();
            this.Close();
            fr.Show();
            fr.TopMost = true;
        }

        private async void button4_Click(object sender, EventArgs e)
        {
            //brisanje gosta
            if(listBox1.SelectedItem == null)
            {
                MessageBox.Show("Niste odabrali gosta.");
                return;
            }
            var id = int.Parse(listBox1.SelectedItem.ToString().Split(' ')[0]);

            var result = await client.DeleteAsync("api/gost/" + id);
            if (result.StatusCode.ToString() == "OK")
            {
                MessageBox.Show("Success");
            }
            else
            {
                MessageBox.Show("Unsuccessful");

            }
            GostiForm fr = new GostiForm();
            this.Close();
            fr.Show();
            fr.TopMost = true;
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            var odabir = listBox1.SelectedItem.ToString().Split(' ');
            textBox1.Text = odabir[1];
            textBox2.Text = odabir[2];
        }

        private async void button2_Click(object sender, EventArgs e)
        {
            //azuriranje
            if(listBox1.SelectedItem == null)
            {
                MessageBox.Show("Nije odabran gost.");
                return;
            }
            int id = int.Parse(listBox1.SelectedItem.ToString().Split(' ')[0]);

            var obj = new { ime = textBox1.Text, prezime = textBox2.Text, id = id };

            var json = JsonConvert.SerializeObject(obj);
            var a = new StringContent(json, Encoding.UTF8, "application/json");
            var result = await client.PutAsync("api/gost", a);
            if (result.StatusCode.ToString() == "OK")
            {
                MessageBox.Show("Success");
            }
            else
            {
                MessageBox.Show("Unsuccessful");
                return;
            }

            GostiForm fr = new GostiForm();
            this.Close();
            fr.Show();
            fr.TopMost = true;

        }
    }
}
