﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IznajmiAPI.DbModels
{
    public partial class Gost
    {
        public Gost()
        {
            Rezervacijas = new HashSet<Rezervacija>();
        }

        public int IdGost { get; set; }
        public string Ime { get; set; }
        public string Prezime { get; set; }

        public virtual ICollection<Rezervacija> Rezervacijas { get; set; }
    }
}
