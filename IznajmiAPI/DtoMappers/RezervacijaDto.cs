﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IznajmiAPI.DtoMappers
{
    public static class RezervacijaDto
    {
        public static DbModels.Rezervacija RezervacijaJsonToDatabase(JObject json)
        {
            DbModels.Rezervacija dbRezervacija = new DbModels.Rezervacija();

            var datumOd = json["datum_od"].ToObject<DateTime>();
            var datumDo = json["datum_do"].ToObject<DateTime>();
            var userId = json["user_id"].ToObject<int>();
            var sobaId = json["soba_id"].ToObject<int>();
            int? rezervacijaId = json["rezervacija_id"].ToObject<int?>();

            dbRezervacija.DatumDo = datumDo;
            dbRezervacija.DatumOd = datumOd;
            dbRezervacija.IdSoba = sobaId;
            dbRezervacija.IdGost = userId;

            if (rezervacijaId != null)
            {
                dbRezervacija.IdRezervacija = (int)rezervacijaId;
                return dbRezervacija;
            }
            else
            {
                dbRezervacija.IdRezervacija = 0;
                return dbRezervacija;
            }
        }
    }
}
