﻿using System;
using System.Collections.Generic;
using System.Text;
using DesktopApp.Models;

namespace DesktopApp.Models
{
    public class Soba
    {
        public int? Id_soba { get; set; }
        public int BrojSobe { get; set; }
        public int Id_objekt_foreignKey { get; set; }

        public Models.Objekt Objekt { get; set; }

        public Soba(int? id_soba, int brojsobe, int id_objekt_fk, Models.Objekt o)
        {
            this.Id_soba = id_soba;
            this.BrojSobe = brojsobe;
            this.Id_objekt_foreignKey = id_objekt_fk;
            this.Objekt = o;

        }
    }
}
