﻿using IznajmiAPI.Services;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IznajmiAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class Gost : Controller
    {
        private GostService _gostService;

        public Gost(GostService gostService)
        {
            _gostService = gostService;
        }

        [HttpGet]
        public ActionResult<List<Models.Gost>> Get()
        {
            return _gostService.DohvatiGosteService().ToList();
        }

        [HttpPost]
        public void Post([FromBody] JObject json)
        {
            _gostService.DodavanjeGosta(DtoMappers.GostDto.FromJson(json));
        }

        [HttpDelete("{id}")]
        public void DeleteGost(string id)
        {
            _gostService.Brisanje(int.Parse(id));
        }

        [HttpPut]
        public void Azuriranje([FromBody] JObject json)
        {
            _gostService.Azuriranje(DtoMappers.GostDto.FromJson(json));
        }
    }
}