﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IznajmiAPI.DbModels;
using IznajmiAPI.Mapper;

namespace IznajmiAPI.Repositories
{
    public class GostRepository
    {
        private readonly dbRezervacijeContext _dbContext;

        public GostRepository(dbRezervacijeContext c)
        {
            _dbContext = c;
        }

        public IEnumerable<Models.Gost> DohvatiGoste()
        {
            return _dbContext.Gosts.Select(gost => GostMapper.FromDatabase(gost));
        }

        public void DodajGosta(DbModels.Gost g)
        {
            _dbContext.Gosts.Add(g);
            _dbContext.SaveChanges();
        }

        public void BrisanjeGosta(int id)
        {
            DbModels.Gost dbGost = _dbContext.Gosts.Where(x => x.IdGost == id).Single();

            _dbContext.Gosts.Remove(dbGost);
            _dbContext.SaveChanges();
        }
        public void Azuriranje(DbModels.Gost gost)
        {
            _dbContext.Gosts.Update(gost);
            _dbContext.SaveChanges();
        }
    }
}